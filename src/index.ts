import express from 'express'

const app = express()

app.get('/', (req: express.Request, res: express.Response) => {
    res.send('Hell world')
})

app.listen(3000, () => {
    console.log('Server started');
})